package bcas.ap.dp.singleton.dbs;

import java.sql.SQLException;

public class DBDemo {

	public static void main(String[] args) {
	
		JDBCSingleton jdbc=JDBCSingleton.getTnstance();
		System.out.println("Data inserted");
		
		try {
			jdbc.inserData("Meena",20);
			jdbc.inserData("Raja",21);
			jdbc.inserData("Raven",15);
			jdbc.inserData("Karthi",26);
			jdbc.readData();
		}catch (SQLException e) {
			e.printStackTrace();
		}

	}

}

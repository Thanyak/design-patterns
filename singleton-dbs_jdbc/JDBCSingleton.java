package bcas.ap.dp.singleton.dbs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JDBCSingleton {
	
	
	//create a JDBCSingleton class.
	//static members holds only one instance of the JDBCSingleton class.
	private static JDBCSingleton jdbc;
	
	//JDBCSingleton prevent the instantiation from any other class.
	private JDBCSingleton() {
	}
	
	//Here we provides global point of access.
	public static JDBCSingleton getTnstance() {
		if(jdbc==null) {
			jdbc=new JDBCSingleton();
		}
		return jdbc;
	}
	
	//To get connection from methods like insert data/ view data etc.
	private Connection getConnection()throws ClassNotFoundException,SQLException{
		Connection connection=null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/csd16","root","root");
		return connection;
	}
	
	//To insert the record into the database.
	public void inserData(String name, int age)throws SQLException{
		Connection connection=null;
		PreparedStatement statement=null;
		
		try{
			connection=this.getConnection();
		//	statement=connection.prepareStatement("INSERT INTO student(name,age)VALUES(2,Bala)");
			statement=connection.prepareStatement("INSERT INTO student(name,age)VALUES(?,?)");
			
			statement.setString(1, name);
			statement.setInt(2, age);
			statement.executeUpdate();
		}catch (ClassNotFoundException|SQLException e) {
			e.printStackTrace();
		}finally {
			if (connection !=null) {
				connection.close();
			}
			if(statement !=null) {
				statement.close();
			}
		}
		
	}
	

	//To read the record from the database.
	public void readData() throws SQLException {
		Connection connection=null;
		PreparedStatement statement=null;
		ResultSet resultSet=null;
	
		try {
			connection=this.getConnection();
			statement=connection.prepareStatement("SELECT * FROM student");
		
			resultSet=statement.executeQuery();
			System.out.println("ID\tName\t\tAge");
			while (resultSet.next()){
				System.out.println(resultSet.getString(1)+"\t"+resultSet.getString(2)+"\t"+"\t"+resultSet.getString(3));
			}
						
		}catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			if(resultSet !=null) {
				resultSet.close();
			}
			if(connection !=null) {
				connection.close();
			}
			if(statement !=null) {
				statement.close();
			}
		}
		
	}
	
	//To read the record from the database with search parm.
	public void readData(String searchText) throws SQLException {
		Connection connection=null;
		PreparedStatement statement=null;
		ResultSet resultSet=null;
	
		try {
			connection=this.getConnection();
			statement=connection.prepareStatement("SELECT * FROM student WHERE name=?");
			statement.setString(1, searchText);
			resultSet=statement.executeQuery();
			System.out.println("ID\tName");
			while (resultSet.next()){
				System.out.println(resultSet.getString(1)+"\t"+resultSet.getString(2));
			}
						
		}catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			if(resultSet !=null) {
				resultSet.close();
			}
			if(connection !=null) {
				connection.close();
			}
			if(statement !=null) {
				statement.close();
			}
		}
		
	}
	
	public void updateData(String name, int age) throws SQLException {
		Connection connection=null;
		PreparedStatement statement=null;
	
		try {
			connection=this.getConnection();
			statement=connection.prepareStatement("UPDATE student SET age=? WHERE name=?");
			statement.setString(2, name);
			statement.setInt(1, age);
			statement.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			if(connection !=null) {
				connection.close();
			}
			if(statement !=null) {
				statement.close();
			}
		}
	
		
	}
}

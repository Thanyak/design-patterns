package bcas.adp.dp.singleton.dbs;

import java.sql.SQLException;

public class DBManipulationDemo {

	public static void main(String[] args) {
		DBQuery dbQuery=DBQuery.getTnstance();
		System.out.println("Data inserted");
		
		
		try {
			dbQuery.insertData("Thanya", 26);
			dbQuery.deleteData("Thanya");
			
			dbQuery.insertData("Kannan", 55);
			dbQuery.updateData("Kannan", 56);
		
			dbQuery.insertData("Malathy", 55);
			
			
			dbQuery.insertData("Nikitha",23);
			dbQuery.readData();
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
		

	
	}

}

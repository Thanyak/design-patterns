package bcas.adp.dp.singleton.dbs;

public class DBConstants {

	public static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	public static final String HOST="localhost";
	public static final int PORT =3306;
	public static final String DB_Name="csd16";
	public static final String DB_USER_NAME="root";
	public static final String DB_PASSWORD="root";

}

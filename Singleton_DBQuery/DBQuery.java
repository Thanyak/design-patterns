package bcas.adp.dp.singleton.dbs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBQuery implements DataManipulation {
	
	private static DBQuery dbQuery;
	
	private DBQuery() {
	
	}
	
	public static DBQuery getTnstance() {
		if(dbQuery==null) {
		dbQuery=new DBQuery();
		}
		return dbQuery;
	}
	

	@Override
	public void insertData(String name, int age) throws SQLException {
		Connection connection=null;
		PreparedStatement statement=null;
	
		try {
			connection=JDBC_Connector.getConnection();
			statement=connection.prepareStatement("INSERT INTO student(name,age)VALUES(?,?)");
			statement.setString(1, name);
			statement.setInt(2, age);
			statement.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			if(connection !=null) {
				connection.close();
			}
			if(statement !=null) {
				statement.close();
			}
		}
	}

	@Override
	public void readData(String searchText) throws SQLException {
		Connection connection=null;
		PreparedStatement statement=null;
		ResultSet resultSet=null;
	
		try {
			connection=JDBC_Connector.getConnection();
			statement=connection.prepareStatement("SELECT * FROM student WHERE name=?");
			statement.setString(1, searchText);
			resultSet=statement.executeQuery();
			System.out.println("ID\tName");
			while (resultSet.next()){
				System.out.println(resultSet.getString(1)+"\t"+resultSet.getString(2));
			}
						
		}catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			if(resultSet !=null) {
				resultSet.close();
			}
			if(connection !=null) {
				connection.close();
			}
			if(statement !=null) {
				statement.close();
			}
		}
		
	}





	@Override
	public void updateData(String name, int age) throws SQLException {
		Connection connection=null;
		PreparedStatement statement=null;
	
		try {
			connection=JDBC_Connector.getConnection();
			statement=connection.prepareStatement("UPDATE student SET age=? WHERE name=?");
			statement.setInt(1, age);
			statement.setString(2, name);
			statement.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			if(connection !=null) {
				connection.close();
			}
			if(statement !=null) {
				statement.close();
			}
		}
	
		
	}

	@Override
	public void deleteData(String name) throws SQLException {
		Connection connection=null;
		PreparedStatement statement=null;
	
		try {
			connection=JDBC_Connector.getConnection();
			statement=connection.prepareStatement("DELETE FROM student WHERE name=?");
			statement.setString(1, name);
			statement.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			if(connection !=null) {
				connection.close();
			}
			if(statement !=null) {
				statement.close();
			}
		}
	
		
	}

	@Override
	public void readData() throws SQLException {
		Connection connection=null;
		PreparedStatement statement=null;
		ResultSet resultSet=null;
	
		try {
			connection=JDBC_Connector.getConnection();
			statement=connection.prepareStatement("SELECT * FROM student");
		
			resultSet=statement.executeQuery();
			System.out.println("ID\tName\t\tAge");
			while (resultSet.next()){
				System.out.println(resultSet.getString(1)+"\t"+resultSet.getString(2)+"\t"+"\t"+resultSet.getString(3));
			}
						
		}catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			if(resultSet !=null) {
				resultSet.close();
			}
			if(connection !=null) {
				connection.close();
			}
			if(statement !=null) {
				statement.close();
			}
		}
		
	}


}

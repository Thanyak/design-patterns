package bcas.adp.dp.singleton.dbs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBC_Connector {

	public static Connection getConnection()throws ClassNotFoundException, SQLException {
		Connection connection=null;
		Class.forName(DBConstants.DRIVER);
		String connection_string="jdbc:mysql://"+DBConstants.HOST+":"+DBConstants.PORT+"/"+DBConstants.DB_Name;
		connection=DriverManager.getConnection(connection_string, DBConstants.DB_USER_NAME,DBConstants.DB_PASSWORD);
		return connection;
	}
}

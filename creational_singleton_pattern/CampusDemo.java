package bcas.ap.dp.creational.singleton;

public class CampusDemo {

	public static void main(String[] args) {
		
		Campus Jaffna = Campus.CreateInstance();
		Jaffna.setName("Jaffna");
		System.out.println(Jaffna+" "+Jaffna.hashCode());

		Campus Colombo = Campus.CreateInstance();
		Colombo.setName("Colombo");
		System.out.println(Colombo+" "+Colombo.hashCode());

	}

}

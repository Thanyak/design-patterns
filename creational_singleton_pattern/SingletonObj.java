package bcas.ap.dp.creational.singleton;

public class SingletonObj {
	
	private static SingletonObj singletonObj;
	
	private SingletonObj() {
		
	}

	public static SingletonObj getInstance() {
		if (singletonObj==null) {
			singletonObj=new SingletonObj();
		}
		return singletonObj;
	}
	public static void setNull() {
		singletonObj=null;
	}
}

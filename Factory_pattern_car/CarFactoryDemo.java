package bcas.ap.dp.factory.car;

public class CarFactoryDemo {

	public static void main(String[] args) {
		
		CarFactory.buildCar(CarType.TOYATO, CarColour.BLACK);
		System.out.println("-------------------------------");
		CarFactory.buildCar(CarType.AUDI, CarColour.BLUE);
		System.out.println("-------------------------------");
		CarFactory.buildCar(CarType.KIA, CarColour.RED);
		


	}

}

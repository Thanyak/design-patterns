package bcas.ap.dp.factory.car;

public class Kia extends Car {

	private Object colour;

	public Kia(CarColour colour) {
		super(CarType.KIA);
		this.colour=colour;
		assembel();
		paint();
	}


	@Override
	void assembel() {
		System.out.println("Assembel Audi Car");
		
	}

	@Override
	void paint() {
		System.out.println("Painted colour is "+ colour);
		
	}

}

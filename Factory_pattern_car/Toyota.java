package bcas.ap.dp.factory.car;

public class Toyota extends Car {

	private Object colour;

	public Toyota(CarColour colour) {
		super(CarType.TOYATO);
		this.colour=colour;
		assembel();
		paint();
	
	}

	@Override
	void assembel() {
		System.out.println("Assembel Toyato Car");
		
	}

	@Override
	void paint() {
		System.out.println("Painted colour is "+colour);
		
	}

}

package bcas.ap.dp.factory.car;

public class Audi extends Car{
	private Object colour;
	
	public Audi( CarColour colour) {
		super(CarType.AUDI);
		this.colour=colour;
		assembel();
		paint();
	}


	@Override
	void assembel() {
		System.out.println("Assembel Audi Car");
	}

	@Override
	void paint() {
		System.out.println("Painted colour is "+colour);
	}
}

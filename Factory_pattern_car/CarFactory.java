package bcas.ap.dp.factory.car;

public class CarFactory {
	public static Car buildCar(CarType type,CarColour colour) {
		switch(type) {
		case TOYOTA:
			return new Toyota(colour);
		case AUDI:
			return new Audi(colour);
		case KIA:
			return new Kia(colour);
		default:
			break;
		}
		return null;
	}

}

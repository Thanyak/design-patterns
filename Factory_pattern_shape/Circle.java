package bcas.ap.dp.factory.shape;

public class Circle implements Shape{

	@Override
	public void draw() {
		System.out.println("Draw method from circle");
	}
	
	public float getareaofCircle(double radius) {
		double area = Math.PI * radius * radius;
		return (float) area;
	}
	
	public float getpremeterofCircle(double radius) {
		double perimeter = 2 * Math.PI * radius;
		return (float) perimeter;
	}
	
	@Override
	public double getArea(double w, double h) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getPerimeter(double w, double h) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getedgef() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getareaofPentagon(int a, int s) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getpremeterofPentagon(int a, int s) {
		// TODO Auto-generated method stub
		return 0;
	}




}

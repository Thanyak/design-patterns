package bcas.ap.dp.factory.shape;

public interface Shape {
	public void draw();
	public double getArea(double w,double h);
	public double getPerimeter(double w,double h);
	public int getedgef();
	public float getareaofCircle(double radius);
	public float getpremeterofCircle(double radius);
	public float getareaofPentagon(int a, int s);
	public float getpremeterofPentagon(int a, int s);
}

package bcas.ap.dp.factory.shape;

public class ShapeFactoryDemo {

	public static void main(String[] args) {
		
	
		
		ShapeFactory shapefactory=new ShapeFactory();
	
		
		Shape circle=shapefactory.callShape(ShapeType.CIRCLE);
		circle.draw();
				System.out.println(" - Area of Circle is: "+circle.getareaofCircle(7)+"\n"+" - Perimeter of circle is: "+circle.getpremeterofCircle(7)+"\n");
		
		Shape square=shapefactory.callShape(ShapeType.SQUARE);
		square.draw();
				System.out.println(" - Area of square is: "+square.getArea(4,4)+"\n"+" - Perimeter of square is: "+square.getPerimeter(4,4)+"\n"+
					" - Edgef of square : "+square.getedgef()+"\n");
				
		Shape rectangle=shapefactory.callShape(ShapeType.RECTANGLE);
		rectangle.draw();
				System.out.println(" - Area of Rectangle is: "+rectangle.getArea(4, 4)+"\n"+" - Perimeter of rectangle is: "+rectangle.getPerimeter(6,4)+"\n"+
				" - Edgef of rectangle : "+rectangle.getedgef()+"\n");
		
		Shape pentagon=shapefactory.callShape(ShapeType.PENTAGON);
		pentagon.draw();
						System.out.println(" - Area of Petagon is: "+pentagon.getareaofPentagon(13, 5)+"\n"+" - Perimeter of pentagon is: "+
						pentagon.getpremeterofPentagon(13, 5)+"\n"+" - Edgef of pentagon : "+pentagon.getedgef()+"\n");
				

	}

}

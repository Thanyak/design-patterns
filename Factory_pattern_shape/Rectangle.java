package bcas.ap.dp.factory.shape;

public class Rectangle implements Shape{

	@Override
	public void draw() {
		System.out.println("Draw method from Rectangle");
		
	}

	@Override
	public double getArea(double w, double h) {
		
		return w*h;
	}

	@Override
	public double getPerimeter(double w, double h) {
	
		return (w+h)*2;
	}

	@Override
	public int getedgef() {
	
		return 4;
	}

	@Override
	public float getareaofCircle(double radius) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getpremeterofCircle(double radius) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getareaofPentagon(int a, int s) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getpremeterofPentagon(int a, int s) {
		// TODO Auto-generated method stub
		return 0;
	}


}
